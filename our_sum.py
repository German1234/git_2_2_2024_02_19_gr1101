from numbers import Number


def my_sum(num1: Number, num2: Number) -> Number:
    return num1 + num2
