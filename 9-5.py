# Створіть функцію quadratic_equation, яка приймає на вхід 3 параметри: a, b, c. Усередині цієї функції створити
# змінні x1, x2 зі значенням None (спочатку приймаємо, що рівняння не має коренів) та функцію calc_rezult з формальними
# параметрами зовнішньої функції quadratic_equation. Всередині функції calc_rezult
# здійснити пошук дискримінанта, згідно з результатом якого зробити розрахунок коренів рівняння. Зовнішня функція
# quadratic_equation має повернути перелік значень коренів квадратного рівняння.


def quadratic_equation(a, b, c):
    x1, x2 = None, None

    def calc_result(a, b, c):
        nonlocal x1, x2
        d = b ** 2 - 4 * a * c

        if d > 0:
            x1 = int((-b + d ** 0.5) / (2 * a))
            x2 = int((-b - d ** 0.5) / (2 * a))
        elif d == 0:
            x1 = x2 = -b / (2 * a)
        else:
            return None, None

    calc_result(a, b, c)
    return x1, x2


a = float(input("Enter coefficient a: "))
b = float(input("Enter coefficient b: "))
c = float(input("Enter coefficient c: "))

result = quadratic_equation(a, b, c)
print(f"Roots of the quadratic equation: {result}")
