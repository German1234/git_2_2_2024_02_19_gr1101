from numbers import Number


def square(num: Number) -> Number:
    """Return a square of a number."""
    return num ** 2


